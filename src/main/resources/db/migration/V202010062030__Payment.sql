DROP TABLE IF EXISTS "payment","payment";
CREATE TABLE "payment"."payment"
(
    "id"             serial primary key,
    "external_id" varchar NOT NULL,
    "payee_merchant_id" varchar NOT NULL,
    "description" varchar,
    "currency" varchar NOT NULL,
    "price" NUMERIC NOT NULL,
    "failure_reason" varchar,
    "created_at" timestamp NOT NULL,
    "paid_at" timestamp,
    "method" varchar NOT NULL,
    "intent" varchar NOT NULL,
    "state" varchar NOT NULL
)

package com.example.springsocial.service;


import com.example.springsocial.model.dto.PayOrderDto;
import com.paypal.api.payments.Payment;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PaymentService {

	com.example.springsocial.model.Payment successPay(String paymentId, String token, String payerId);

	public Payment pay(PayOrderDto payOrderDto, String successUrl,String cancelUrl);

	Optional<com.example.springsocial.model.Payment> cancelPay(String paymentId, String token, HttpServletRequest request);

	public Payment executePayment(String paymentId, String payerId);

	Payment payOrder( String successUrl, String cancelUrl, UUID id);

	List<com.example.springsocial.model.Payment> findAllPaymentByOrderId(UUID orderId);
}

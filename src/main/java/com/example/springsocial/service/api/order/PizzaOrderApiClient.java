package com.example.springsocial.service.api.order;

import com.example.springsocial.model.dto.OrderDto;
import com.example.springsocial.model.dto.PizzaOrderDto;
import com.example.springsocial.model.enumerated.OrderState;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

import java.util.UUID;

public interface PizzaOrderApiClient {

	@Headers("Accept: application/json")
	@GET("/api/v1/order/{id}")
	Call<OrderDto> findOrderById(@Path("id") UUID order);

	@Headers("Accept: application/json")
	@POST("/api/v1/order/{id}/changeStatus")
	Call<Void> changeOrderStatus(@Path("id") UUID order, @Query("state") OrderState state);
}

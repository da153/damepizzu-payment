package com.example.springsocial.service.impl;

import com.example.springsocial.config.PaypalConfig;
import com.example.springsocial.engine.PaymentManager;
import com.example.springsocial.exception.BadRequestException;
import com.example.springsocial.model.dto.OrderDto;
import com.example.springsocial.model.dto.PayOrderDto;
import com.example.springsocial.model.enumerated.OrderState;
import com.example.springsocial.repository.PaymentRepository;
import com.example.springsocial.service.PaymentService;
import com.example.springsocial.service.api.order.PizzaOrderApiClient;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.PaymentExecution;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Service
@AllArgsConstructor
@Slf4j
public class PaymentServiceImpl implements PaymentService {

	private final PaypalConfig paypalConfig;

	private final PaymentManager paymentManager;
	private final PizzaOrderApiClient apiClient;
	private final PaymentRepository paymentRepository;

	@Override
	public com.example.springsocial.model.Payment successPay(String paymentId, String token, String payerId) {
		Payment payment = executePayment(paymentId, payerId);
		Optional<com.example.springsocial.model.Payment> byId = paymentRepository.findByExternalId(paymentId);
		paymentManager.saveSuccessPayment(payment);
		try {
			if (byId.isPresent()) {
				Call<Void> call = apiClient.changeOrderStatus(byId.get().getOrderId(), OrderState.PAID);
				Response<Void> response = null;
				try {
					response = call.execute();
				} catch (IOException e) {
					throw new BadRequestException("Order cannot be found");
				}
				if (!response.isSuccessful()) {
					throw new BadRequestException("Order ids are not valid, or item does not exists");
				}
				if (payment.getState().equals("approved")) {
					return byId.get();
				}
				throw new BadRequestException("Payment is not approved");
			} else {
				throw new BadRequestException("Bad payment id");
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		throw new BadRequestException("Cannot save internal payment success");
	}

	@Override
	public Payment pay(PayOrderDto payOrderDto, String successUrl, String cancelUrl) {
		try {
			APIContext apiContext = paypalConfig.generateNewAPIContext();

			Payment payment = paymentManager.createPayment(successUrl, cancelUrl, null)
				.create(apiContext);
			try {
				paymentManager.createNewPaymentEntity(payOrderDto, payment);
			} catch (Exception e) {
				return payment;
			}
			return payment;
		} catch (PayPalRESTException e) {
			e.printStackTrace();
			throw new BadRequestException("Cannot create payment", e);
		}
	}

	@Override
	public Optional<com.example.springsocial.model.Payment> cancelPay(String paymentId, String token, HttpServletRequest request) {
		return paymentManager.saveCancelPayment(paymentId);
	}

	public Payment executePayment(String paymentId, String payerId) {
		Payment payment = new Payment();
		payment.setId(paymentId);
		PaymentExecution paymentExecute = new PaymentExecution();
		paymentExecute.setPayerId(payerId);
		try {
			APIContext apiContext = paypalConfig.generateNewAPIContext();
			return payment.execute(apiContext, paymentExecute);
		} catch (PayPalRESTException e) {
			paymentManager.saveCancelPayment(paymentId);
			e.printStackTrace();
			throw new BadRequestException("Cannot execute payment", e);
		}
	}

	@Override
	public Payment payOrder(String successUrl, String cancelUrl, UUID orderId) {
		Call<OrderDto> orderById = apiClient.findOrderById(orderId);
		Response<OrderDto> response = null;
		try {
			response = orderById.execute();
		} catch (IOException e) {
			throw new BadRequestException("Order cannot be found");
		}
		if (!response.isSuccessful()) {
			throw new BadRequestException("Order ids are not valid, or item does not exists");
		}
		OrderDto pizzaOrderDto = response.body();
		try {
			APIContext apiContext = paypalConfig.generateNewAPIContext();
			Payment payment = paymentManager.createPayment(successUrl, cancelUrl, pizzaOrderDto)
				.create(apiContext);
			try {
				paymentManager.createNewPaymentEntit(payment, orderId, pizzaOrderDto);
			} catch (Exception e) {
				throw new BadRequestException("Order does not exists");
			}
			return payment;
		} catch (PayPalRESTException e) {
			e.printStackTrace();
			throw new BadRequestException("Cannot create payment", e);
		}
	}

	@Override
	public List<com.example.springsocial.model.Payment> findAllPaymentByOrderId(UUID orderId) {
		return paymentRepository.findAllPaymentByOrderId(orderId);
	}

}

package com.example.springsocial.security;

import com.example.springsocial.config.TokenAuthorizationClient;
import com.example.springsocial.exception.BadRequestException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;

@Service
@AllArgsConstructor
public class AuthorizationService {

	private final TokenAuthorizationClient tokenAuthorizationClient;

	public AccountDto getAccountFromToken(String token) {
		Call<AccountDto> call = tokenAuthorizationClient.getMyAccountFromToken(token);
		try {
			Response<AccountDto> response = call.execute();
			if (response.isSuccessful()) {
				return response.body();
			}
			throw new BadRequestException("Cannot get user by this token!");
		} catch (Exception e) {
			throw new BadRequestException("Cannot get user by this token!");
		}
	}
}

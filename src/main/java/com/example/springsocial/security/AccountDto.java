package com.example.springsocial.security;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AccountDto {
	private Integer id;
	private String username;
	private Role role;
	private String created;
	private String lastLogin;
	private String firstName;
	private String lastName;
	private String email;
	private String phone;

}

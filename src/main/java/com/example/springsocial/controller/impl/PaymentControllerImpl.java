package com.example.springsocial.controller.impl;

import com.example.springsocial.controller.PaymentController;
import com.example.springsocial.model.dto.PayOrderDto;
import com.example.springsocial.service.PaymentService;
import com.example.springsocial.service.api.order.MicroserviceApiConfiguration;
import com.example.springsocial.util.URLUtils;
import com.paypal.api.payments.Links;
import com.paypal.api.payments.Payment;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@AllArgsConstructor
@Slf4j
public class PaymentControllerImpl implements PaymentController {

	private final PaymentService paymentService;

	private final MicroserviceApiConfiguration apiConfig;

	@Override
	public ResponseEntity<String> pay(PayOrderDto payOrderDto,
	                                  HttpServletRequest request,
	                                  HttpServletResponse response) {
		String coreUrl = URLUtils.getBaseURl(request);
		String successUrl = String.format("%s%s", coreUrl, PAYPAL_SUCCESS_URL);
		String cancelUrl = String.format("%s%s", coreUrl, PAYPAL_CANCEL_URL);
		Payment payment = paymentService.pay(payOrderDto, successUrl, cancelUrl);
		for (Links links : payment.getLinks()) {
			if (links.getRel()
				.equals("approval_url")) {
				log.info(links.getHref());
				return ResponseEntity.ok(links.getHref());
			}
		}

		return ResponseEntity.badRequest()
			.body("");
	}

	@Override
	public void cancelPay(String paymentId, String token, HttpServletRequest request, HttpServletResponse res) {
		try {
			Optional<com.example.springsocial.model.Payment> payment = paymentService.cancelPay(paymentId, token, request);

			URIBuilder b = null;
			try {
				if (payment.isPresent()) {
					b = new URIBuilder(String.format("%s/order/%s/payment/error", apiConfig.getAdmin(), payment.get().getOrderId()));
				} else {
					b = new URIBuilder(String.format("%s/order/payment/error", apiConfig.getAdmin()));
					b.addParameter("paymentId", paymentId);
				}
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
			try {
				if (b != null) {
					res.sendRedirect(b.toString());
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			try {
				res.sendRedirect(String.format("%s/order/payment/error",apiConfig.getAdmin()));
			} catch (IOException ex) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void successPay(String paymentId, String token, String payerId, HttpServletResponse res) {
		try {
			com.example.springsocial.model.Payment payment = paymentService.successPay(paymentId, token, payerId);
			URIBuilder b = null;
			try {
				if (payment != null) {
					b = new URIBuilder(String.format("%s/order/%s/payment/success", apiConfig.getAdmin(), payment.getOrderId()));
					b.addParameter("paymentId", paymentId);
					b.addParameter("payerId", payerId);
				}
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
			res.sendRedirect(b.toString());
		} catch (Exception e) {
			e.printStackTrace();
			URIBuilder b = null;
			try {
				b = new URIBuilder(String.format("%s/order/payment/error", apiConfig.getAdmin()));
				res.sendRedirect(b.toString());
			} catch (URISyntaxException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}

		}
	}

	@Override
	public HttpEntity<String> payOrder(HttpServletRequest request, HttpServletResponse response, UUID id) {
		String coreUrl = URLUtils.getBaseURl(request);
		String successUrl = String.format("%s%s", coreUrl, PAYPAL_SUCCESS_URL);
		String cancelUrl = String.format("%s%s", coreUrl, PAYPAL_CANCEL_URL);
		Payment payment = paymentService.payOrder(successUrl, cancelUrl, id);
		for (Links links : payment.getLinks()) {
			if (links.getRel()
				.equals("approval_url")) {
				log.info(links.getHref());
				return ResponseEntity.ok(links.getHref());
			}
		}

		return ResponseEntity.badRequest()
			.body("");
	}

	@Override
	public List<com.example.springsocial.model.Payment> findAllPaymentByOrderId(UUID orderId) {
		return  paymentService.findAllPaymentByOrderId(orderId);
	}

}

package com.example.springsocial.controller;

import com.example.springsocial.model.Payment;
import com.example.springsocial.model.dto.PayOrderDto;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@Validated
public interface PaymentController {
	String PAY_URL = "/api/payment/pay";
	String PAYPAL_SUCCESS_URL = "/api/payment/pay/success";
	String PAYPAL_CANCEL_URL = "/api/payment/pay/cancel";
	String PAY_ORDER_URL = "/api/payment/pay/order/{id}";
	String FIND_PAYMENT_BY_ORDERID = "/api/v1/payment/findAllPaymentByOrderId/{id}";

	@PostMapping(PAY_URL)
	public ResponseEntity pay(@Valid @RequestBody PayOrderDto payOrderDto, HttpServletRequest request, HttpServletResponse response);

	@GetMapping(PAYPAL_CANCEL_URL)
	public void cancelPay(@RequestParam(value = "paymentId", required = false) String paymentId, @RequestParam(value = "token", required = false) String token, HttpServletRequest request, HttpServletResponse res);

	@GetMapping(PAYPAL_SUCCESS_URL)
	public void successPay(@RequestParam("paymentId") String paymentId, @RequestParam("token") String token, @RequestParam("PayerID") String payerId, HttpServletResponse response);

	@PostMapping(PAY_ORDER_URL)
	public HttpEntity<String> payOrder(HttpServletRequest request,
	                                   HttpServletResponse response,
	                                   @PathVariable UUID id);

	@GetMapping(FIND_PAYMENT_BY_ORDERID)
	public List<Payment> findAllPaymentByOrderId(@PathVariable("id") UUID orderId);

}

package com.example.springsocial.engine;

import com.example.springsocial.model.dto.OrderDto;
import com.example.springsocial.model.dto.PayOrderDto;
import com.paypal.api.payments.Payment;

import java.util.Optional;
import java.util.UUID;

public interface PaymentManager {

	void saveSuccessPayment(Payment payment);
	Optional<com.example.springsocial.model.Payment> saveCancelPayment(String paymentId);
	com.example.springsocial.model.Payment createNewPaymentEntity(PayOrderDto payOrderDto, Payment payment);

	Payment createPayment(String successUrl, String cancelUrl, OrderDto pizzaOrderDto);

	com.example.springsocial.model.Payment createNewPaymentEntity(PayOrderDto payOrderDto,
	                                                              Payment payment,
	                                                              UUID orderId);

	com.example.springsocial.model.Payment createNewPaymentEntit(Payment payment, UUID orderId, OrderDto pizzaOrderDto);

}

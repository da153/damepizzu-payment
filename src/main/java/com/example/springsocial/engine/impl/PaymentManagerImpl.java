package com.example.springsocial.engine.impl;

import com.example.springsocial.engine.PaymentManager;
import com.example.springsocial.mapper.PaymentMapper;
import com.example.springsocial.model.dto.OrderDto;
import com.example.springsocial.model.dto.PayOrderDto;
import com.example.springsocial.model.enumerated.PaymentState;
import com.example.springsocial.repository.PaymentRepository;
import com.paypal.api.payments.Amount;
import com.paypal.api.payments.Item;
import com.paypal.api.payments.ItemList;
import com.paypal.api.payments.Payer;
import com.paypal.api.payments.Payment;
import com.paypal.api.payments.RedirectUrls;
import com.paypal.api.payments.Transaction;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class PaymentManagerImpl implements PaymentManager {

	private final PaymentRepository paymentRepository;
	private final PaymentMapper paymentMapper;


	public void saveSuccessPayment(Payment payment) {
		paymentRepository.findByExternalId(payment.getId())
			.ifPresent(p -> {
				p.setPaidAt(LocalDateTime.now());
				p.setState(PaymentState.CHARGED);
				paymentRepository.save(p);
			});
	}

	public Optional<com.example.springsocial.model.Payment> saveCancelPayment(String paymentId) {
		paymentRepository.findByExternalId(paymentId)
			.ifPresent(p -> {
				p.setState(PaymentState.CANCELED);
				paymentRepository.save(p);
			});
		return paymentRepository.findByExternalId(paymentId);
	}

	@Override
	public com.example.springsocial.model.Payment createNewPaymentEntity(PayOrderDto payOrderDto, Payment payment) {
		com.example.springsocial.model.Payment dbPayment = paymentMapper.createPayment(payOrderDto, payment);
		return paymentRepository.save(dbPayment);
	}

	public Payment createPayment(String successUrl,
	                             String cancelUrl,
	                             OrderDto pizzaOrderDto) {
		Amount amount = new Amount("CZK",
			String.format("%.2f",
					BigDecimal.valueOf(pizzaOrderDto.getTotalPrice())
						.setScale(2, RoundingMode.HALF_UP)
						.doubleValue()
				)
				.replace(",", ".")
		);
		Transaction transaction = new Transaction();
		transaction.setDescription("Platba v obchodě DamePizzu.cz");
		transaction.setAmount(amount);

		List<Item> items = pizzaOrderDto.getOrderItems()
			.stream()
			.map(o -> {
				Item item = new Item();
				item.setCurrency("CZK");
				item.setName(o.getName());
				item.setPrice(o.getPrice()
					.toString());
				item.setQuantity(o.getAmount()
					.toString());
				return item;
			}).collect(Collectors.toList());
		ItemList itemList = new ItemList();
		itemList.setItems(items);
		transaction.setItemList(itemList);

		List<Transaction> transactions = new ArrayList<>();
		transactions.add(transaction);

		Payer payer = new Payer();
		payer.setPaymentMethod("paypal");

		Payment payment = new Payment();
		payment.setIntent("sale");
		payment.setPayer(payer);
		payment.setTransactions(transactions);
		RedirectUrls redirectUrls = new RedirectUrls();
		redirectUrls.setCancelUrl(cancelUrl);
		redirectUrls.setReturnUrl(successUrl);
		payment.setRedirectUrls(redirectUrls);
		return payment;
	}

	@Override
	public com.example.springsocial.model.Payment createNewPaymentEntity(PayOrderDto payOrderDto, Payment payment, UUID orderId) {
		com.example.springsocial.model.Payment dbPayment = paymentMapper.createPayment(payOrderDto, payment);
		dbPayment.setOrderId(orderId);
		return paymentRepository.save(dbPayment);
	}

	@Override
	public com.example.springsocial.model.Payment createNewPaymentEntit(Payment payment,
	                                                                    UUID orderId,
	                                                                    OrderDto pizzaOrderDto) {
		com.example.springsocial.model.Payment dbPayment = paymentMapper.createPayment(pizzaOrderDto, payment);
		dbPayment.setOrderId(orderId);
		return paymentRepository.save(dbPayment);	}


}

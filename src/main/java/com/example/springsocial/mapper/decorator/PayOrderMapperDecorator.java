package com.example.springsocial.mapper.decorator;

import com.example.springsocial.mapper.PaymentMapper;
import com.example.springsocial.model.Payment;
import com.example.springsocial.model.dto.OrderDto;
import com.example.springsocial.model.dto.PayOrderDto;
import com.example.springsocial.model.enumerated.PaymentState;
import com.example.springsocial.model.enumerated.PaypalPaymentIntent;
import com.example.springsocial.model.enumerated.PaypalPaymentMethod;

import java.time.LocalDateTime;

public abstract class PayOrderMapperDecorator implements PaymentMapper {

	@Override
	public Payment createPayment(OrderDto orderDto, com.paypal.api.payments.Payment paypal) {
		Payment payment = new Payment();
		payment.setCreatedAt(LocalDateTime.now());
		payment.setPrice(orderDto.getTotalPrice());
		payment.setDescription("Platba");
		payment.setIntent(PaypalPaymentIntent.valueOf(paypal.getIntent()));
		payment.setCurrency("CZK");
		payment.setMethod(PaypalPaymentMethod.valueOf("paypal"));
		payment.setState(PaymentState.CREATED);
		payment.setExternalId(String.valueOf(paypal.getId()));
		return payment;
	}

	@Override
	public Payment createPayment(PayOrderDto payOrderDto, com.paypal.api.payments.Payment paypalPayment) {
		Payment payment = new Payment();
		payment.setCreatedAt(LocalDateTime.now());
		payment.setPrice(payOrderDto.getPrice());
		payment.setDescription(payOrderDto.getDescription());
		payment.setIntent(payOrderDto.getIntent());
		payment.setCurrency(payOrderDto.getCurrency());
		payment.setMethod(payOrderDto.getMethod());
		payment.setState(PaymentState.CREATED);
		payment.setExternalId(payment.getExternalId());
		return payment;
	}

}

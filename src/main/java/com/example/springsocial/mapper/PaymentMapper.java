package com.example.springsocial.mapper;

import com.example.springsocial.mapper.decorator.PayOrderMapperDecorator;
import com.example.springsocial.model.Payment;
import com.example.springsocial.model.dto.OrderDto;
import com.example.springsocial.model.dto.PayOrderDto;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
@DecoratedWith(PayOrderMapperDecorator.class)
public interface PaymentMapper {

	@Mapping(target = "intent", ignore = true)
	Payment createPayment(PayOrderDto payOrderDto, com.paypal.api.payments.Payment payment);

	@Mapping(target = "id", ignore = true)
	@Mapping(target = "state", ignore = true)
	Payment createPayment(OrderDto orderDto, com.paypal.api.payments.Payment payment);
}

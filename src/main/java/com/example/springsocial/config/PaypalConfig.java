package com.example.springsocial.config;

import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.OAuthTokenCredential;
import com.paypal.base.rest.PayPalRESTException;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
@Data
public class PaypalConfig {

	@Value("${app.paypal.client.id}")
	private String clientId;
	@Value("${app.paypal.client.secret}")
	private String clientSecret;
	@Value("${app.paypal.mode}")
	private String mode;

	@Bean
	public Map<String, String> paypalSdkConfig() {
		Map<String, String> configMap = new HashMap<>();
		configMap.put("mode", mode);
		return configMap;
	}

	@Bean
	public OAuthTokenCredential oAuthTokenCredential() {
		return new OAuthTokenCredential(clientId, clientSecret, paypalSdkConfig());
	}

	public APIContext generateNewAPIContext() {
		APIContext context = null;
		try {
			context = new APIContext(oAuthTokenCredential().getAccessToken());
		} catch (PayPalRESTException e) {
			e.printStackTrace();
		}
		context.setConfigurationMap(paypalSdkConfig());
		return context;
	}

	@Bean
	public APIContext apiContext() throws PayPalRESTException {
		APIContext context = new APIContext(oAuthTokenCredential().getAccessToken());
		context.setConfigurationMap(paypalSdkConfig());
		return context;
	}

}

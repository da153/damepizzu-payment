package com.example.springsocial.config;

import com.example.springsocial.security.UserPrincipal;
import com.example.springsocial.service.api.order.MicroserviceApiConfiguration;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import lombok.AllArgsConstructor;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.lang.reflect.Type;
import java.time.LocalDateTime;

@Configuration
@AllArgsConstructor
public class TokenClientConfig {

	private final MicroserviceApiConfiguration apiConfiguration;

	@Bean
	public TokenAuthorizationClient createAuthorizationClient() {
		HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
		logging.setLevel(HttpLoggingInterceptor.Level.BODY);

		Gson gson = new GsonBuilder().registerTypeAdapter(LocalDateTime.class, new JsonDeserializer<LocalDateTime>() {
			@Override
			public LocalDateTime deserialize(JsonElement json, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
				return LocalDateTime.parse(json.getAsJsonPrimitive()
					.getAsString());
			}
		}).create();

		OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
			@Override
			public Response intercept(Chain chain) throws IOException {
				try {
					UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext()
						.getAuthentication()
						.getPrincipal();
					Request.Builder newRequest  = chain.request().newBuilder();
					if (userPrincipal != null) {
						newRequest.addHeader("Authorization", userPrincipal.getToken());
					}
					return chain.proceed(newRequest.build());
				} catch (Exception e) {
					return chain.proceed(chain.request());
				}
			}
		}).addInterceptor(logging).build();


		Retrofit retrofit = new Retrofit.Builder()
			.client(
				client
			)
			.baseUrl(apiConfiguration.getAuth())
			.addConverterFactory(GsonConverterFactory.create(gson))
			.build();
		return retrofit.create(TokenAuthorizationClient.class);
	}

}

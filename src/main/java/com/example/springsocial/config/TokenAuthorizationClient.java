package com.example.springsocial.config;

import com.example.springsocial.security.AccountDto;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface TokenAuthorizationClient {

	@GET("/api/account/me")
	Call<AccountDto> getMyAccountFromToken(@Header("Authorization") String token);

}

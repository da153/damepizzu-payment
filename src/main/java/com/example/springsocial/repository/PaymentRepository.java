package com.example.springsocial.repository;

import com.example.springsocial.model.Payment;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PaymentRepository extends BaseRepository<Payment, Integer> {

	Optional<Payment> findByExternalId(String id);

	List<Payment> findAllPaymentByOrderId(UUID orderId);
}

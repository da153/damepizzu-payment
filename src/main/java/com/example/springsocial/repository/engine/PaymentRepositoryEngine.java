package com.example.springsocial.repository.engine;

import com.example.springsocial.model.Payment;
import org.hibernate.jpa.TypedParameterValue;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PaymentRepositoryEngine extends CrudRepository<Payment, Integer> {

	Optional<Payment> findByExternalId(String id);

	@Query(value = "SELECT p from Payment p WHERE p.orderId = :orderId")
	List<Payment> findAllByOrderId(UUID orderId);
}

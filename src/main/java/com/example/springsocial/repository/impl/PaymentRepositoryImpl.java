package com.example.springsocial.repository.impl;

import com.example.springsocial.model.Payment;
import com.example.springsocial.repository.PaymentRepository;
import com.example.springsocial.repository.engine.PaymentRepositoryEngine;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.jpa.TypedParameterValue;
import org.hibernate.type.PostgresUUIDType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
@Slf4j
@AllArgsConstructor
public class PaymentRepositoryImpl extends AbstractRepository<Payment, Integer> implements PaymentRepository {

	private final PaymentRepositoryEngine engine;


	@Override
	protected CrudRepository<Payment, Integer> getRepositoryEngine() {
		return engine;
	}

	@Override
	public Optional<Payment> findByExternalId(String id) {
		return engine.findByExternalId(id);
	}

	@Override
	public List<Payment> findAllPaymentByOrderId(UUID orderId) {
		return engine.findAllByOrderId(orderId);
	}

}

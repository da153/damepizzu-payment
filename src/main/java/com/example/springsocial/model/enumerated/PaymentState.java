package com.example.springsocial.model.enumerated;

public enum PaymentState {
	CREATED, IN_PROGRESS, CANCELED, REJECTED, CHARGED, BAD_PAYMENT;
}


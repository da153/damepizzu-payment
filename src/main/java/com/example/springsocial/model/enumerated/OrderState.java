package com.example.springsocial.model.enumerated;

public enum OrderState {
	CREATED, DONE, COOKING, DELIVERY, PAID, CHECKED, CANCELED
}

package com.example.springsocial.model;

import com.example.springsocial.model.enumerated.PaymentState;
import com.example.springsocial.model.enumerated.PaypalPaymentIntent;
import com.example.springsocial.model.enumerated.PaypalPaymentMethod;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Payment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String externalId;

	private String description;

	private String currency;

	private double price;

	private String failureReason;

	private LocalDateTime createdAt;

	private LocalDateTime paidAt;

	@Enumerated(value = EnumType.STRING)
	private PaypalPaymentMethod method;

	@Enumerated(value = EnumType.STRING)
	private PaypalPaymentIntent intent;

	@Enumerated(value = EnumType.STRING)
	private PaymentState state;

	@Type(type="org.hibernate.type.PostgresUUIDType")
	private UUID orderId;

}

package com.example.springsocial.model.dto;

import com.example.springsocial.model.enumerated.PaypalPaymentIntent;
import com.example.springsocial.model.enumerated.PaypalPaymentMethod;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PayOrderDto {

	private double price;

	private String currency;

	private String description;

	private PaypalPaymentMethod method;

	private PaypalPaymentIntent intent;

}

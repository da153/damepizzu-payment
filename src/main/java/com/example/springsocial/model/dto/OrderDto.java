package com.example.springsocial.model.dto;

import com.example.springsocial.model.enumerated.OrderState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDto {

	private UUID id;

	private Double totalPrice;

	private OrderCustomer customer;

	private OrderState state;

	private List<OrderItemDto> orderItems;

}

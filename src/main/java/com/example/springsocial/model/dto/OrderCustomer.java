package com.example.springsocial.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderCustomer {

	private int customerId;

	private String customerFullname;

	private String customerZip;

	private String customerAddress;

	private String customerCity;

	private String customerEmail;

	private String customerPhone;

}
